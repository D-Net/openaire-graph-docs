# Sorting and paging

The OpenAIRE Graph API allows you to sort and page through the results of your search queries. 
This enables you to retrieve the most relevant results and manage large result sets more effectively.

## Sorting
Sorting based on specific fields, helps to retrieve data in the preferred order. 
Sorting is achieved using the `sortBy` parameter, which specifies the field and the direction (ascending or descending) for sorting.

* `sortBy`: Defines the field and the sort direction. The format should be `fieldname sortDirection`, where the `sortDirection` can be either `ASC` for ascending order or `DESC` for descending order.

The field names that can be used for sorting are specific to each entity type and can be found in the `sortBy` field values of the [available paremeters](../searching-entities/filtering-search-results.md#available-parameters).

Note that the default sorting is based on the `relevance` score of the search results.

Examples:

- Get research products published after `2020-01-01` and sort them by the publication date in descending order:

    [https://api-beta.openaire.eu/graph/researchProducts?fromPublicationDate=2020-01-01&sortBy=publicationDate DESC](https://api-beta.openaire.eu/graph/researchProducts?fromPublicationDate=2020-01-01&sortBy=publicationDate%20DESC)

- Get research products with the keyword `"COVID-19"` and sort them by their (citation-based) popularity:

    [https://api-beta.openaire.eu/graph/researchProducts?search=COVID-19&sortBy=popularity DESC](https://api-beta.openaire.eu/graph/researchProducts?search=COVID-19&sortBy=popularity%20DESC)

Note that you can combine multiple sorting conditions by separating them with a comma.

Example: 

- Get research products with the keyword `"COVID-19"` and sort them by their publication date in ascending order and then by their popularity in descending order:

    [https://api-beta.openaire.eu/graph/researchProducts?search=COVID-19&sortBy=publicationDate ASC,popularity DESC](https://api-beta.openaire.eu/graph/researchProducts?search=COVID-19&sortBy=publicationDate%20ASC,popularity%20DESC)

## Paging 

The OpenAIRE Graph API supports paging through the use of `page` and `pageSize` parameters, enabling you to specify which part of the result set to retrieve and how many results per page.

* `page`: Specifies the page number of the results you want to retrieve. Page numbering starts from 1.

* `pageSize`: Defines the number of results to be returned per page. This helps limit the amount of data returned in a single request, making it easier to process.

Example:

- Get the top 10 most influential research products that contain the phrase "knowledge graphs":

    [https://api-beta.openaire.eu/graph/researchProducts?search="knowledge graphs"&page=1&pageSize=10&sortBy=influence DESC](https://api-beta.openaire.eu/graph/researchProducts?search=%22knowledge%20graphs%22&page=1&pageSize=10&sortBy=influence%20DESC)