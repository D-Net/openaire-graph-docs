# Graph API <span class="theme-doc-version-badge badge badge--secondary">beta</span>


The OpenAIRE Graph API provides a comprehensive way for developers to explore the [OpenAIRE Graph](https://graph.openaire.eu/), a vast interconnected dataset that aggregates metadata from a wide range of scholarly resources. 
The Graph API offers endpoints for accessing and querying this interconnected dataset, enabling users to retrieve detailed information on research products, data sources, organizations, and projects. 

## Base URL and Swagger documentation

The base URL of the Graph API is:
```
https://api-beta.openaire.eu/graph/
```

You can access the API Swagger documentation in [https://api-beta.openaire.eu/graph/swagger-ui/index.html#/](https://api-beta.openaire.eu/graph/swagger-ui/index.html#/).

## Notes
Please note that the Graph API: 

- is intended for data discovery and exploration, hence you are now allowed to navigate the full result set: you are limited to the first 10,000 results of a search query. If you are interested to access the whole graph, we encourage you to download the [OpenAIRE full Graph dataset](../../downloads/full-graph.md).

- adhers to the [terms of use](../terms.md) of the OpenAIRE public APIs - certain (rate limit) restrictions apply.

## Learn more

Please use the following links to learn more about the Graph API:

- [Getting a single entity](./getting-a-single-entity.md) - Retrieve detailed information on a single entity.
- [Searching entities](./searching-entities/searching-entities.md) - Retrieve a list of entities based on specific search criteria.
    - [Filtering results](./searching-entities/filtering-search-results.md) - Filter search results based on specific criteria.
    - [Sorting results](./searching-entities/sorting-and-paging.md#sorting) - Sort search results based on specific criteria.
    - [Paging](./searching-entities/sorting-and-paging.md#paging) - Retrieve a subset of search results. 
- [Making requests](./making-requests.md) - Learn how to make requests with different programming languages.
