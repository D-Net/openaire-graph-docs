# Version 2.0


<a href="https://api.scholexplorer.openaire.eu/v2/ui" ><h3>Swagger API v2.0</h3> </a>

 The Scholexplorer version 2.0 APIs offer a Swagger entry point that allows access to all the relationships within the OpenAIRE graph. 

The response format for requests to the Links endpoint complies with the <a href="https://zenodo.org/records/1120275"> Scholix Schema Version 2.0 </a>. The Scholix framework consists of a conceptual and information model containing standards, aspirational principles, practical guidelines, and options for encoding and exchange protocols to support linking between scholarly publications and related research data.  It provides a standardized way of representing metadata related to these links, facilitating the exchange and integration of information about connections between publications, datasets, and other research entities.  This allows for a more comprehensive and interoperable approach to understanding and exploring the relationships within the research landscape.

By using the Links endpoint and the Scholix response format, users can easily explore and analyze the relationships within the Scholixplorer graph, gaining a complete and detailed view of the connections between the various research resources.
 



