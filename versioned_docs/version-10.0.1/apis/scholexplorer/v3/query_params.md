# Query Parameters

<a href="https://api.scholexplorer.openaire.eu/v3/ui" ><h3>Swagger API v3.0</h3> </a>


Scholexplorer models relationships as links between pairs of research products, defined as **source** and **target**. Links are further characterized by the **type** of the two products (publication, dataset, software, or other), the **date** of publishing (typically the date of the source product), the **publisher** of the two products (a string), and the **PID** of the products (or the OpenAIRE ID, useful when the product does not come with a PID). 

More specifically, here is the list of API parameters:

**Required parameters (at least one):**

- **sourcePid** Represents the unique identifier (PID) of the source entity in the relationship. This parameter is used to specify which entity is initiating or originating the relationship.
- **targetPid** Represents the unique identifier (PID) of the target entity in the relationship. This parameter is used to specify the entity that is receiving or being affected by the relationship.
- **sourcePublisher** Identifies the publisher of the source entity.
- **targetPublisher** Identifies the publisher of the target entity.
- **sourceType** The typology of the source entity like: Publication, Dataset, Software Other
- **targetType** The typology of the target entity like: Publication, Dataset, Software Other

**Optional parameters:**
- **relation** Defines the type of relationship between the source and target entities.
- **from**: Filter all relationships where the date is greater than or equal to the specified date
- **to**: Filter all relationships where the date is lower than or equal to the specified date