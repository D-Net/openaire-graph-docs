# Searching entities

This is a guide on how to search for specific entities using the OpenAIRE Graph API.

## Endpoints

Currently, the Graph API supports the following entity types:
* Research products - endpoint: [`GET /researchProducts`](https://api.openaire.eu/graph/v1/researchProducts)
* Organizations - endpoint: [`GET /organizations`](https://api.openaire.eu/graph/v1/organizations)
* Data sources - endpoint: [`GET /dataSources`](https://api.openaire.eu/graph/v1/dataSources)
* Projects - endpoint: [`GET /projects`](https://api.openaire.eu/graph/v1/projects)

Each of these endpoints can be used to list all entities of the corresponding type.
Listing such entities can be more useful when using the [filtering](./filtering-search-results.md), 
[sorting](./sorting-and-paging.md#sorting), and [paging](./sorting-and-paging.md#paging) capabilities of the Graph API.

## Response

The response of the aforementioned endpoints is an object of the following type: 

```json
{
    header: {
        numFound: 36818386,
        maxScore: 1,
        queryTime: 21,
        page: 1,
        pageSize: 10
    },
    results: [
        ...
    ]
}
```

It contains a `header` object with the following fields:
- `numFound`: the total number of entities found
- `maxScore`: the maximum relevance score of the search results
- `queryTime`: the time in milliseconds that the search took
- `page`: the current page of the search results (when using basic pagination)
- `pageSize`: the number of entities per page
- `nextCursor`: the next page cursor (when using cursor-based pagination, see: [paging](./sorting-and-paging.md#paging)

Finally, the `results` field contains an array of entities of the corresponding type (i.e., [Research product](../../../data-model/entities/research-product.md), [Organization](../../../data-model/entities/organization.md), [Data Source](../../../data-model/entities/data-source.md), or [Project](../../../data-model/entities/project.md)).
