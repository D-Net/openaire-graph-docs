---
sidebar_position: 2
---

# Beginner's kit

The large size of the OpenAIRE Research Graph is a major impediment for beginners to familiarise with the underlying data model and explore its contents. 
Working with the Graph in its full size typically requires access to a huge distributed computing infrastructure which cannot be easily accessible to everyone.
[The OpenAIRE Beginner’s Kit]( https://doi.org/10.5281/zenodo.7490192) aims to address this issue. It consists of two components:

* A subset of the Graph composed of the research products published between 2022-06-29 and 2022-12-29, all the entities connected to them and the respective relationships.
* A Zeppelin notebook that demonstrates how you can use PySpark to analyse the Graph and get answers to some interesting research questions. 