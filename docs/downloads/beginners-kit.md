---
sidebar_position: 2
---

# Beginner's kit

Despite the wealth of data contained in the OpenAIRE Graph, its sheer size may initially appear daunting, potentially hindering its widespread adoption. To address this challenge, we created the **OpenAIRE Beginner’s Kit**, a user-friendly solution providing access to a subset of the OpenAIRE Graph within a sandboxed environment coupled with a Jupyter notebook for analysis.
This solution has been described in this [paper](https://doi.org/10.5281/zenodo.13942506), which was presented at the [28th International Conference on Science, Technology and Innovation Indicators (STI2024)](https://sti2024.org), held in Berlin, Germany, on 18-20 September 2024.


[The OpenAIRE Beginner’s Kit](https://doi.org/10.5281/zenodo.10841263) aims to address this issue. It consists of two components:

* A subset of the Graph composed of the research products published between 2024-06-01 and 2024-12-31, together with all the entities connected to them and the respective relationships. The dataset can be found on Zenodo [here](https://doi.org/10.5281/zenodo.7490191)
* A Jupyter notebook that demonstrates how you can use PySpark to analyse the Graph and get answers to some interesting research questions.  

To install, please follow the instructions on the [GitHub repository](https://github.com/openaire/beginners-kit).

:::note How to cite
Mannocci, A., & Baglioni, M. (2024, September 19). Exploring Scientometrics with the OpenAIRE Graph: Introducing the OpenAIRE Beginner's Kit. 28th International Conference on Science, Technology and Innovation Indicators (STI2024), Berlin, Germany. https://doi.org/10.5281/zenodo.13942507
:::