---
sidebar_position: 1
---

# Research products

Research products are intended as digital objects, described by metadata, resulting from a scientific process.
In this page, we descibe the properties of the `ResearchProduct` object.

Moreover, there are the following sub-types of a `ResearchProduct`, that inherit all its properties and further extend it:
* [Publication](#publication)
* [Data](#data)
* [Software](#software)
* [Other research product](#other-research-product)

--- 

## The `ResearchProduct` object 

### id
_Type: String &bull; Cardinality: ONE_

Main entity identifier, created according to the [OpenAIRE entity identifier and PID mapping policy](../pids-and-identifiers).

```json
"id": "doi_dedup___::80f29c8c8ba18c46c88a285b7e739dc3"
```

### type
_Type: String  &bull; Cardinality: ONE_

Type of the research products. Possible types: 

* `publication`
* `data`
* `software`
* `other`

as declared in the terms from the [dnet:result_typologies vocabulary](https://api.openaire.eu/vocabularies/dnet:result_typologies).

```json
"type": "publication"
```

### originalIds
_Type: String &bull; Cardinality: MANY_

Identifiers of the record at the original sources.

```json
"originalIds": [
    "oai:pubmedcentral.nih.gov:8024784",
    "S0048733321000305",
    "10.1016/j.respol.2021.104226",
    "3136742816"
]
```

### mainTitle
_Type: String &bull; Cardinality: ONE_

A name or title by which a research product is known. It may be the title of a publication or the name of a piece of software.

```json
"mainTitle": "The fall of the innovation empire and its possible rise through open science"
```

### subTitle

_Type: String &bull; Cardinality: ONE_

Explanatory or alternative name by which a research product is known.

```json
"subTitle": "An analysis of cases from 1980 - 2020"
```

### authors
_Type: [Author](other#author) &bull; Cardinality: MANY_

The main researchers involved in producing the data, or the authors of the publication.

```json
"authors": [
    {
        "fullName": "E. Richard Gold",
        "rank": 1, 
        "name": "Richard",
        "surname": "Gold",
        "pid": {
            "id": {
                "scheme": "orcid",
                "value": "0000-0002-3789-9238" 
            },
            "provenance": {
                "provenance": "Harvested",
                "trust": "0.9" 
            }
        }
    }, 
    ...
]
```
### bestAccessRight
_Type: [BestAccessRight](other#bestaccessright) &bull; Cardinality: ONE_

The most open access right associated to the manifestations of this research product.

```json
"bestAccessRight": {
    "code": "c_abf2",
    "label": "OPEN",
    "scheme": "http://vocabularies.coar-repositories.org/documentation/access_rights/"
}
```

### contributors
_Type: String &bull; Cardinality: MANY_

The institution or person responsible for collecting, managing, distributing, or otherwise contributing to the development of the resource.

```json
"contributors": [
    "University of Zurich",
    "Wright, Aidan G C",
    "Hallquist, Michael", 
    ...
]
```

### countries
_Type: [ResultCountry](other#resultcountry) &bull; Cardinality: MANY_

Country associated with the research product: it is the country of the organisation that manages the institutional repository or national aggregator or CRIS system from which this record was collected.
Country of affiliations of authors can be found instead in the affiliation relation.

```json
"countries": [
    {
        "code": "CH",
        "label": "Switzerland",
        "provenance": {
            "provenance": "Inferred by OpenAIRE",
            "trust": "0.85"
        }
    }, 
    ...
]
```

### coverages
_Type: String &bull; Cardinality: MANY_

###  dateOfCollection
_Type: String &bull; Cardinality: ONE_

When OpenAIRE collected the record the last time. 

```json
"dateOfCollection": "2021-06-09T11:37:56.248Z"
```

### descriptions
_Type: String &bull; Cardinality: MANY_

A brief description of the resource and the context in which the resource was created.

```json
"descriptions": [
    "Open science partnerships (OSPs) are one mechanism to reverse declining efficiency. OSPs are public-private partnerships that openly share publications, data and materials.",
    "There is growing concern that the innovation system's ability to create wealth and attain social benefit is declining in effectiveness. This article explores the reasons for this decline and suggests a structure, the open science partnership, as one mechanism through which to slow down or reverse this decline.",
    "The article examines the empirical literature of the last century to document the decline. This literature suggests that the cost of research and innovation is increasing exponentially, that researcher productivity is declining, and, third, that these two phenomena have led to an overall flat or declining level of innovation productivity.", 
    ...
]
```

### embargoEndDate
_Type: String &bull; Cardinality: ONE_

Date when the embargo ends and this research product turns Open Access.

```json
"embargoEndDate": "2017-01-01"
```

### indicators
_Type: [Indicator](other#indicator-1) &bull; Cardinality: ONE_

The indicators computed for this research product;
currently, the following types of indicators are supported: 

* [Citation-based impact indicators by BIP!](other#citationimpact)
* [Usage Statistics indicators](other#usagecounts)

```json
"indicators": {
        "citationImpact": {
                "influence": 123,
                "influenceClass": "C2",
                "citationCount": 456,
                "citationClass": "C3",
                "popularity": 234,
                "popularityClass": "C1",
                "impulse": 987,
                "impulseClass": "C3"
        },
        "usageCounts": {
                "downloads": "10",
                 "views": "20"
        }
}
```

### instances
_Type: [Instance](other#instance) &bull; Cardinality: MANY_

Specific materialization or version of the research product. For example, you can have one research product with three instances: one is the pre-print, one is the post-print, one is the published version.

```json
"instances": [
    {
        "accessRight": {
            "code": "c_abf2",
            "label": "OPEN",
            "openAccessRoute": "gold",
            "scheme": "http://vocabularies.coar-repositories.org/documentation/access_rights/"
        },
        "alternateIdentifiers": [
            {
                "scheme": "doi",
                "value": "10.1016/j.respol.2021.104226"
            },
            ...
        ],
        "articleProcessingCharge": {
            "amount": "4063.93",
            "currency": "EUR"
        },
        "license": "http://creativecommons.org/licenses/by-nc/4.0",
        "pids": [
            {
                "scheme": "pmc",
                "value": "PMC8024784"
            },
            ...
        ],
        
        "publicationDate": "2021-01-01",
        "refereed": "UNKNOWN",
        "type": "Article",
        "urls": [
            "http://europepmc.org/articles/PMC8024784"
        ]
    },
    ...
]
```

### language
_Type: [Language](other#language) &bull; Cardinality: ONE_

The alpha-3/ISO 639-2 code of the language. Values controlled by the [dnet:languages vocabulary](https://api.openaire.eu/vocabularies/dnet:languages).

```json
"language": {
    "code": "eng",
    "label": "English"
}
```
### lastUpdateTimeStamp
_Type: Long &bull; Cardinality: ONE_

Timestamp of last update of the record in OpenAIRE.

```json
"lastUpdateTimeStamp": 1652722279987
```

### pids
_Type: [ResultPid](other#resultpid) &bull; Cardinality: MANY_

Persistent identifiers of the research product. See also the [OpenAIRE entity identifier and PID mapping policy](../pids-and-identifiers) to learn more.

```json
"pids": [
    {
        "scheme": "pmc",
        "value": "PMC8024784"
    },
    {
        "scheme": "doi",
        "value": "10.1016/j.respol.2021.104226"
    },
    ...
]
```

### publicationDate
_Type: String &bull; Cardinality: ONE_

Main date of the research product: typically the publication or issued date. In case of a research product with different versions with different dates, the date of the research product is selected as the most frequent well-formatted date. If not available, then the most recent and complete date among those that are well-formatted. For statistics, the year is extracted and the research product is counted only among the research products of that year. Example: Pre-print date: 2019-02-03, Article date provided by repository: 2020-02, Article date provided by Crossref: 2020, OpenAIRE will set as date 2019-02-03, because it’s the most recent among the complete and well-formed dates. If then the repository updates the metadata and set a complete date (e.g. 2020-02-12), then this will be the new date for the research product because it becomes the most recent most complete date. However, if OpenAIRE then collects the pre-print from another repository with date 2019-02-03, then this will be the “winning date” because it becomes the most frequent well-formatted date.

```json
"publicationDate": "2021-03-18"
```

### publisher
_Type: String &bull; Cardinality: ONE_

The name of the entity that holds, archives, publishes prints, distributes, releases, issues, or produces the resource.

```json
"publisher": "Elsevier, North-Holland Pub. Co"
```

### sources
_Type: String &bull; Cardinality: MANY_

A related resource from which the described resource is derived. See definition of Dublin Core field [dc:source](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/elements11/source).

```json
"sources": [
      "Research Policy",
      "Crossref",
      ...
]
```

### formats
_Type: String &bull; Cardinality: MANY_

The file format, physical medium, or dimensions of the resource.

```json
"formats": [
    "application/pdf",
    "text/html",
    ...
]
```

### subjects
_Type: [Subject](other#subject) &bull; Cardinality: MANY_

Subject, keyword, classification code, or key phrase describing the resource.

OpenAIRE classifies research products according to the [Field of Science](../../graph-production-workflow/indicators-ingestion/fos-classification.md) 
and [Sustainable Development Goals](../../graph-production-workflow/indicators-ingestion/sdg-classification.md) taxonomies. 
Check out the relative sections to know more.

```json
"subjects": [
    {
        "subject": {
            "scheme": "FOS",
            "value": "01 natural sciences"
        },
        "provenance": {
            "provenance": "inferred by OpenAIRE",
            "trust": "0.85"
        }
    },
    {
        "subject": {
            "scheme": "SDG",
            "value": "2. Zero hunger"
        },
        "provenance": {
            "provenance": "inferred by OpenAIRE",
            "trust": "0.83"
        }
    },
    {
        "subject": {
            "scheme": "keyword",
            "value": "Open science"
        },
        "provenance": {
            "provenance": "Harvested",
            "trust": "0.9"
        }
    },
    ...
]
```

### isGreen
_Type: Boolean &bull; Cardinality: ONE_

Indicates whether or not the scientific result was published following the green open access model.

### openAccessColor
_Type: String &bull; Cardinality: ONE_


Indicates the specific open access model used for the publication; possible value is one of `bronze, gold, hybrid`.

### isInDiamondJournal
_Type: Boolean &bull; Cardinality: ONE_

Indicates whether or not the publication was published in a diamond journal.

### publiclyFunded
_Type: String &bull; Cardinality: ONE_

Discloses whether the publication acknowledges grants from public sources.

--- 

## Sub-types

There are the following sub-types of `Result`. Each inherits all its fields and extends them with the following.

### Publication

Metadata records about research literature (includes types of publications listed [here](http://api.openaire.eu/vocabularies/dnet:result_typologies/publication)).

#### container 
_Type: [Container](other#container) &bull; Cardinality: ONE_

Container has information about the conference or journal where the research product has been presented or published.

```json
"container": {
    "edition": "",
    "iss": "5",
    "issnLinking": "",
    "issnOnline": "1873-7625",
    "issnPrinted": "0048-7333",
    "name": "Research Policy",
    "sp": "12",
    "ep": "22",
    "vol": "50"
}
```
### Data

Metadata records about research data (includes the subtypes listed [here](http://api.openaire.eu/vocabularies/dnet:result_typologies/dataset)).

#### size
_Type: String &bull; Cardinality: ONE_

The declared size of the research data.

```json
"size": "10129818"
```

#### version
_Type: String &bull; Cardinality: ONE_

The version of the research data.

```json
"version": "v1.3"
```

#### geolocations
_Type: [GeoLocation](other#geolocation) &bull; Cardinality: MANY_

The list of geolocations associated with the research data.

```json
"geolocations": [
    {
        "box": "18.569386 54.468973  18.066832 54.83707",
        "place": "Tübingen, Baden-Württemberg, Southern Germany",
        "point": "7.72486 50.1084"
    },
    ...
]
```

### Software

Metadata records about research software (includes the subtypes listed [here](http://api.openaire.eu/vocabularies/dnet:result_typologies/software)).

#### documentationUrls
_Type: String &bull; Cardinality: MANY_

The URLs to the software documentation. 

```json
"documentationUrls": [ 
    "https://github.com/openaire/iis/blob/master/README.markdown",
    ...
]
```

#### codeRepositoryUrl
_Type: String &bull; Cardinality: ONE_

The URL to the repository with the source code.

```json
"codeRepositoryUrl": "https://github.com/openaire/iis"
```

#### programmingLanguage
_Type: String &bull; Cardinality: ONE_

The programming language.

```json
"programmingLanguage": "Java"
```

### Other research product

Metadata records about research products that cannot be classified as research literature, data or software (includes types of products listed [here](http://api.openaire.eu/vocabularies/dnet:result_typologies/other)).

#### contactPeople
_Type: String &bull; Cardinality: MANY_

Information on the person responsible for providing further information regarding the resource.

```json
"contactPeople": [
    "Noémie Dominguez",
    ...    
]
```

#### contactGroups
_Type: String &bull; Cardinality: MANY_

Information on the group responsible for providing further information regarding the resource.

```json
"contactGroups": [
    "Networked Multimedia Information Systems (NeMIS)",
    ...
]
```

#### tools
_Type: String &bull; Cardinality: MANY_

Information about tool useful for the interpretation and/or re-use of the research product.

