# Scholexplorer API
<p align="center">
    <img loading="lazy" alt="Scholexplorer" src={require('./logo.png').default} width="60%" className="img_node_modules-@docusaurus-theme-classic-lib-theme-MDXComponents-Img-styles-module"/>
</p>

## API Overview
<p align="justify">
The Scholexplorer API is designed to help researchers, data scientists, and developers discover and navigate the relationships between research publications, datasets, software in the OpenAIRE Graph. 
</p>
<p align="justify">
The OpenAIRE Graph gathers today the largest collection of relationships between all kinds of research outputs, providing a unique entry point merging the traditional space of citations between scientific articles, with the Open Science space of citations between publications, data, and software. Relationships are aggregated from OpenCitations.net, Crossref.org, DataCite.org and thousands of publication, data, software repositories world-wide. The collection is further expanded with the citation links text-mined by OpenAIRE over a pool of Open Access fullt-text counting today 65Mi entries (and growing). 
</p>
<p align="justify">
The Scholexplorer API provide an unprecedented entry point that will enable deeper insights and a more comprehensive understanding of the citations within the research ecosystem.
</p>

## API Versions
The Scholexplorer API support the discovery of Scholix relationships filtering by the following parameters: source ID, target ID, semantics, publisher, entity type (publication, dataset, software, other), or publishing date. As of today, three API versions area available:

- [version 1.0](./v1/version.md) 
- [version 2.0](./v2/version.md) 
- [version 3.0](./v3/version.md)

While currently in beta, v3 is the future of the Scholexplorer API. It includes the latest features and improvements and will soon become the primary version, with v1 and v2 being phased out. We strongly recommend starting with v3 for any new development.

