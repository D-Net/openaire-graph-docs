# Sustainable Development Goals

## Introduction
The Sustainable Development Goals (SDGs) are a set of 17 interconnected global goals established by the United 
Nations in 2015. 
They serve as a universal call to action to end poverty, protect the planet, and ensure peace and prosperity for 
all by 2030. 
The SDGs are designed to be a blueprint for achieving a better and more sustainable future.

<p align="center">
    <img loading="lazy" alt="Data model" src={require('../../assets/img/sdg.png').default} width="100%" 
className="img_node_modules-@docusaurus-theme-classic-lib-theme-MDXComponents-Img-styles-module"/>
</p>

## The 17 Sustainable Development Goals

1. [**No Poverty**](https://sdgs.un.org/goals/goal1): End poverty in all its forms everywhere.
2. [**Zero Hunger**](https://sdgs.un.org/goals/goal2): End hunger, achieve food security and improved nutrition, and 
   promote sustainable agriculture.
3. [**Good Health and Well-being**](https://sdgs.un.org/goals/goal3): Ensure healthy lives and promote well-being 
   for all at all ages.
4. [**Quality Education**](https://sdgs.un.org/goals/goal4): Ensure inclusive and equitable quality education and 
   promote lifelong learning opportunities for all.
5. [**Gender Equality**](https://sdgs.un.org/goals/goal5): Achieve gender equality and empower all women and girls.
6. [**Clean Water and Sanitation**](https://sdgs.un.org/goals/goal6): Ensure availability and sustainable 
   management of water and sanitation for all.
7. [**Affordable and Clean Energy**](https://sdgs.un.org/goals/goal7): Ensure access to affordable, reliable, 
   sustainable, and modern energy for all.
8. [**Decent Work and Economic Growth**](https://sdgs.un.org/goals/goal8): Promote sustained, inclusive, and 
   sustainable economic growth, full and productive employment, and decent work for all.
9. [**Industry, Innovation, and Infrastructure**](https://sdgs.un.org/goals/goal9): Build resilient infrastructure, 
   promote inclusive and sustainable industrialization, and foster innovation.
10. [**Reduced Inequalities**](https://sdgs.un.org/goals/goal10): Reduce inequality within and among countries.
11. [**Sustainable Cities and Communities**](https://sdgs.un.org/goals/goal11): Make cities and human settlements 
    inclusive, safe, resilient, and sustainable.
12. [**Responsible Consumption and Production**](https://sdgs.un.org/goals/goal12): Ensure sustainable consumption 
    and production patterns.
13. [**Climate Action**](https://sdgs.un.org/goals/goal13): Take urgent action to combat climate change and its impacts.
14. [**Life Below Water**](https://sdgs.un.org/goals/goal14): Conserve and sustainably use the oceans, seas, and 
    marine resources for sustainable development.
15. [**Life on Land**](https://sdgs.un.org/goals/goal15): Protect, restore, and promote sustainable use of 
    terrestrial ecosystems, manage forests sustainably, combat desertification, and halt and reverse land 
    degradation and halt biodiversity loss.
16. [**Peace, Justice, and Strong Institutions**](https://sdgs.un.org/goals/goal16): Promote peaceful and inclusive 
    societies for sustainable development, provide access to justice for all, and build effective, accountable, and 
    inclusive institutions at all levels.
17. [**Partnerships for the Goals**](https://sdgs.un.org/goals/goal17): Strengthen the means of implementation and 
    revitalize the global partnership for sustainable development.

## Application in Classification of Research Products

The SDG taxonomy is used to classify research products based on their relevance to the overarching goals. This 
classification helps in identifying the impact of research on sustainable development and aligning research efforts 
with global priorities. Here’s how it can be applied:

1. **Mapping Research Outputs**: Research outputs such as publications are be mapped to specific SDGs based on their 
   objectives, methodologies, and outcomes.
2. **Evaluating Impact**: The classification allows for the evaluation of the impact of research on achieving the 
   SDGs, helping to highlight contributions to specific goals.
3. **Funding and Collaboration**: Aligning research with SDGs can attract funding from organizations focused on 
   sustainable development and foster collaborations with other researchers and institutions working towards 
   similar goals.
4. **Policy and Decision-Making**: Policymakers can use the classification to identify research that supports 
   sustainable development policies and make informed decisions based on evidence from relevant research.

By integrating the SDG taxonomy into the classification of research products, we can ensure that research efforts 
are directed towards addressing the most pressing global challenges and contributing to a sustainable future.

## Conclusion

The Sustainable Development Goals provide a comprehensive framework for addressing global challenges. By applying 
the SDG taxonomy to classify research products, we can better understand and enhance the impact of research on 
sustainable development, ensuring that scientific advancements contribute to a more equitable and sustainable world.

Check an example of how the SDG classification appears in the OpenAIRE data in the 
[data model](../../data-model/entities/research-product#subjects) section.